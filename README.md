Dante (theme for Omeka S)
=========================

> __New versions of this module and support for Omeka S version 3.0 and above
> are available on [GitLab], which seems to respect users and privacy better
> than the previous repository.__

[Dante] is a theme for [Omeka S] designed for the digital repository of student
works and thesis [dante] of [Université de Toulouse Jean-Jaurès].


Installation
------------

The module uses external libraries, so use the release zip to install it, or use
and init the source.

* From the zip

Download the last release [Dante.zip] from the list of releases (the
master does not contain the dependency), and uncompress it in the `modules`
directory.

* From the source and for development:

```sh
composer install --no-dev
npm install
npx gulp
```

See general end user documentation for [Installing a module].

### Optional dependencies

- Module [Contribute] to manage submission of student works.
- Module [Guest] to manage student account in public side.
- Module [Block Plus] to manage pages.
- Module [Advanced Search] to manage the search.
- Module [Reference] to display facets in the results with the internal adapter.
  It is not needed for external search engines that can manage facets natively.
  It should be at least version 3.4.16.


Warning
-------

Use it at your own risk.

It’s always recommended to backup your files and your databases and to check
your archives regularly so you can roll back if needed.


Troubleshooting
---------------

See online issues on the [theme issues] page on GitLab.


License
-------

### Theme (without logos)

This module is published under the [CeCILL v2.1] license, compatible with
[GNU/GPL] and approved by [FSF] and [OSI].

This software is governed by the CeCILL license under French law and abiding by
the rules of distribution of free software. You can use, modify and/ or
redistribute the software under the terms of the CeCILL license as circulated by
CEA, CNRS and INRIA at the following URL "http://www.cecill.info".

As a counterpart to the access to the source code and rights to copy, modify and
redistribute granted by the license, users are provided only with a limited
warranty and the software’s author, the holder of the economic rights, and the
successive licensors have only limited liability.

In this respect, the user’s attention is drawn to the risks associated with
loading, using, modifying and/or developing or reproducing the software by the
user in light of its specific status of free software, that may mean that it is
complicated to manipulate, and that also therefore means that it is reserved for
developers and experienced professionals having in-depth computer knowledge.
Users are therefore encouraged to load and test the software’s suitability as
regards their requirements in conditions enabling the security of their systems
and/or data to be ensured and, more generally, to use and operate it in the same
conditions as regards security.

The fact that you are presently reading this means that you have had knowledge
of the CeCILL license and that you accept its terms.

### Assets

The logos are not open sourced.

### Libraries

See directory asset/vendor for related copyrigths and licenses.


Copyright
---------

See commits for full list of contributors and copyright of the libraries.

* Copyright Flore Leclerc, Véronica Holguin, Denis Chiron, Daniel Berthereau for [Sempiternelia], 2021-2022 (see [Daniel-KM])


[Dante]: https://gitlab.com/Daniel-KM/Omeka-S-theme-Dante
[Omeka S]: https://omeka.org/s
[dante]: https://dante.univ-tlse2.fr
[Université de Toulouse Jean-Jaurès]: https://www.univ-tlse2.fr
[Installing a module]: https://omeka.org/s/docs/user-manual/modules/#installing-modules
[Advanced Search]: https://gitlab.com/Daniel-KM/Omeka-S-module-AdvancedSearch
[Block Plus]: https://gitlab.com/Daniel-KM/Omeka-S-module-BlockPlus
[Contribute]: https://gitlab.com/Daniel-KM/Omeka-S-module-Contribute
[Guest]: https://gitlab.com/Daniel-KM/Omeka-S-module-Guest
[Reference]: https://gitlab.com/Daniel-KM/Omeka-S-module-Reference
[theme issues]: https://gitlab.com/Daniel-KM/Omeka-S-theme-Dante/issues
[CeCILL v2.1]: https://www.cecill.info/licences/Licence_CeCILL_V2.1-en.html
[GNU/GPL]: https://www.gnu.org/licenses/gpl-3.0.html
[FSF]: https://www.fsf.org
[OSI]: http://opensource.org
[GitLab]: https://gitlab.com/Daniel-KM
[Sempiternelia]: https://sempiternelia.net
[Daniel-KM]: https://gitlab.com/Daniel-KM "Daniel Berthereau"
