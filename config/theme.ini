[info]
name        = "Repository"
description = "Thème pour un entrepôt institutionnel, basé sur le thème Dante (Dépôt et Archivage Numérique des Travaux Étudiants) de l’université Toulouse – Jean Jaurès [https://dante.univ-tlse2.fr]"
license     = "CeCILL-2.1"
author      = "Flore Leclerc, Véronica Holguin, Denis Chiron et Daniel Berthereau pour https://sempiternelia.net"
theme_link  = "https://gitlab.com/Daniel-KM/Omeka-s-theme-repository"
author_link = "https://sempiternelia.net/"
version     = "3.4.10"
omeka_version_constraint = "^4.1.0"

helpers[] = ThemeFunctions

[config]
element_groups.global = "Global Settings"

elements.readme.name = "readme"
elements.readme.type = "Radio"
elements.readme.options.label = "Pour info"
elements.readme.options.value_options.readme = "Les modules Access, Advanced Resource Template, Contribute, Block Plus, Advanced Search et Guest sont recommandés pour bénéficier de toutes les fonctionnalités."
elements.readme.attributes.value = ""

elements.main_color.name = "main_color"
elements.main_color.type = "Omeka\Form\Element\ColorPicker"
elements.main_color.options.element_group = "global"
elements.main_color.options.label = "Main color"
elements.main_color.options.info = "The main color of the theme. The default value is #EA3535."
elements.main_color.attributes.value = "#EA3535"
#elements.main_color.attributes.placeholder = "#EA3535"

elements.accent_color.name = "accent_color"
elements.accent_color.type = "Omeka\Form\Element\ColorPicker"
elements.accent_color.options.element_group = "global"
elements.accent_color.options.label = "Main accent color"
elements.accent_color.options.info = "An accent color to be used on links. The default value is #BA0000."
elements.accent_color.attributes.value = "#BA0000"
#elements.accent_color.attributes.placeholder = "#BA0000"

element_groups.header = "Entête"

elements.nav_depth.name = "nav_depth"
elements.nav_depth.type = "Number"
elements.nav_depth.options.element_group = "header"
elements.nav_depth.options.label = "Top menu max depth"
elements.nav_depth.options.info = "Maximum number of levels to show in the site's top navigation bar. Set to 0 to show all levels."
elements.nav_depth.attributes.id = "nav_depth"
elements.nav_depth.attributes.min = 0
elements.nav_depth.attributes.value = 1

elements.logo.name = "logo"
elements.logo.type = "Omeka\Form\Element\Asset"
elements.logo.options.element_group = "header"
elements.logo.options.label = "Logo"
elements.logo.attributes.id = "logo"

elements.logo_url.name = "logo_url"
elements.logo_url.type = "Text"
elements.logo_url.options.element_group = "header"
elements.logo_url.options.label = "Url du lien du logo (si besoin)"
elements.logo_url.attributes.id = "logo_url"

elements.logo_title.name = "logo_title"
elements.logo_title.type = "Text"
elements.logo_title.options.element_group = "header"
elements.logo_title.options.label = "Titre du lien externe du logo (si besoin)"
elements.logo_title.attributes.id = "logo_title"

elements.header_page_support.name = "header_page_support"
elements.header_page_support.type = "Text"
elements.header_page_support.options.element_group = "header"
elements.header_page_support.options.label = "Page d’aide pour l’entête"
elements.header_page_support.attributes.id = "header_page_support"
elements.header_page_support.attributes.value = "aide"
elements.header_page_support.attributes.placeholder = "aide"

elements.header_page_contact.name = "header_page_contact"
elements.header_page_contact.type = "Text"
elements.header_page_contact.options.element_group = "header"
elements.header_page_contact.options.label = "Page de contact pour l’entête"
elements.header_page_contact.attributes.id = "header_page_contact"
elements.header_page_contact.attributes.value = "contact"
elements.header_page_contact.attributes.placeholder = "contact"

element_groups.footer = "Pied-de-page"

elements.footer_menu.name = "footer_menu"
elements.footer_menu.type = "Omeka\Form\Element\ArrayTextarea"
elements.footer_menu.options.element_group = "footer"
elements.footer_menu.options.label = "Menu pied-de-page sous la forme chemin = libellé éventuel (ou module menu "footer") ou ci-dessous"
elements.footer_menu.options.as_key_value = true
elements.footer_menu.attributes.id = "footer_menu"
elements.footer_menu.attributes.rows = "10"
elements.footer_menu.attributes.value = "mentions-legales = Mentions légales
conditions-de-reutilisation = Conditions de réutilisation"
elements.footer_menu.attributes.placeholder = "mentions-legales = Mentions légales
conditions-de-reutilisation = Conditions de réutilisation"

elements.footer.name = "footer"
elements.footer.type = "Omeka\Form\Element\HtmlTextarea"
elements.footer.options.element_group = "footer"
elements.footer.options.label = "Pied de page"
elements.footer.options.info = "HTML pour le pied de page"
elements.footer.attributes.id = "footer"
elements.footer.attributes.rows = "10"
elements.footer.attributes.value = "Powered by Omeka S"
elements.footer.attributes.class = "block-html full wysiwyg"

element_groups.access = "Accès"

elements.access_check.name = "access_check"
elements.access_check.type = "Radio"
elements.access_check.options.element_group = "access"
elements.access_check.options.label = "Détermination du mode d’accès aux documents"
elements.access_check.options.value_options.auto = "Automatique (module, propriété ou visibilité)"
elements.access_check.options.value_options.module_access = "Module Access"
elements.access_check.options.value_options.curation_access = "Valeur dans la propriété curation:access (« Accès libre », « Accès restreint », « Non consultable »)"
elements.access_check.options.value_options.visibility = "Visibilité public/privé"
elements.access_check.attributes.id = "access_check"
elements.access_check.attributes.value = "auto"

element_groups.contribute = "Contribution"

elements.contribution_valuesuggest_idref_disable.name = "contribution_valuesuggest_idref_disable"
elements.contribution_valuesuggest_idref_disable.type = "Checkbox"
elements.contribution_valuesuggest_idref_disable.options.element_group = "contribute"
elements.contribution_valuesuggest_idref_disable.options.label = "Désactiver Idref dans le dépôt utilisateur"
elements.contribution_valuesuggest_idref_disable.attributes.id = "contribution_valuesuggest_idref_disable"

elements.contribution_disabled_values.name = "contribution_disabled_values"
elements.contribution_disabled_values.type = "Omeka\Form\Element\ArrayTextarea"
elements.contribution_disabled_values.options.element_group = "contribute"
elements.contribution_disabled_values.options.label = "Liste des descripteurs désactivés dans les listes"
elements.contribution_disabled_values.attributes.id = "contribution_disabled_values"

elements.contribution_property_people.name = "contribution_property_people"
elements.contribution_property_people.type = "Omeka\Form\Element\PropertySelect"
elements.contribution_property_people.options.element_group = "contribute"
elements.contribution_property_people.options.label = "Champs à présenter sous la forme de deux champs nom / prénom"
elements.contribution_property_people.options.empty_option = "Select one or multiple properties..."
elements.contribution_property_people.options.term_as_value = true
elements.contribution_property_people.attributes.id = "contribution_property_people"
elements.contribution_property_people.attributes.multiple = "multiple"
elements.contribution_property_people.attributes.class = "chosen-select"
elements.contribution_property_people.attributes.data-placeholder = "Select one or multiple properties..."
elements.contribution_property_people.attributes.value.0 = "dcterms:creator"
elements.contribution_property_people.attributes.value.1 = "dcterms:contributor"

elements.contribution_message_not_submitted.name = "contribution_message_not_submitted"
elements.contribution_message_not_submitted.type = "Laminas\Form\Element\Textarea"
elements.contribution_message_not_submitted.options.element_group = "contribute"
elements.contribution_message_not_submitted.options.label = "HTML pour indiquer qu’un document n’a pas été déposé"
elements.contribution_message_not_submitted.attributes.id = "contribution_message_not_submitted"
elements.contribution_message_not_submitted.attributes.value = 'Une fois le dépot confirmé, l’équipe administrative prendra le temps de vérifier le document. Vous recevrez une notification par mail qui vous indiquera sa validation ou non.'

elements.contribution_message_is_submitted.name = "contribution_message_is_submitted"
elements.contribution_message_is_submitted.type = "Laminas\Form\Element\Textarea"
elements.contribution_message_is_submitted.options.element_group = "contribute"
elements.contribution_message_is_submitted.options.label = "HTML pour indiquer qu’un document est déposé"
elements.contribution_message_is_submitted.attributes.id = "contribution_message_is_submitted"
elements.contribution_message_is_submitted.attributes.value = 'Le document a été déposé.'

element_groups.block_texts = "Texte des blocs de contribution"

elements.contribution_help_aside_template.name = "contribution_help_aside_template"
elements.contribution_help_aside_template.type = "Omeka\Form\Element\HtmlTextarea"
elements.contribution_help_aside_template.options.element_group = "block_texts"
elements.contribution_help_aside_template.options.label = "HTML pour l’aide de la contribution (type)"
elements.contribution_help_aside_template.attributes.id = "contribution_help_aside_template"
elements.contribution_help_aside_template.attributes.rows = "12"
elements.contribution_help_aside_template.attributes.value = '<aside class="aside-interrogation">
    <div class="aside-content">
        <h4>Étape 1 : Sélectionnez le type de document</h4>
        <p class="aside-download-link">
            <a href="">Télécharger l’aide</a>
        </p>
        <p class="aside-more-link">
            <a href="">En savoir plus</a>
        </p>
    </div>
</aside>
'

elements.contribution_help_aside_notice.name = "contribution_help_aside_notice"
elements.contribution_help_aside_notice.type = "Omeka\Form\Element\HtmlTextarea"
elements.contribution_help_aside_notice.options.element_group = "block_texts"
elements.contribution_help_aside_notice.options.label = "HTML pour l’aide de la contribution (notice)"
elements.contribution_help_aside_notice.attributes.id = "contribution_help_aside_notice"
elements.contribution_help_aside_notice.attributes.rows = "12"
elements.contribution_help_aside_notice.attributes.value = '<aside class="aside-interrogation">
    <div class="aside-content">
        <h4>Étape 2 : Saisissez les informations correspondant à votre dépôt</h4>
        <p class="aside-download-link">
            <a href="">Télécharger l’aide</a>
        </p>
        <ul>
            <li><span>Les informations doivent être saisies en minuscules avec la 1ère lettre en majuscule selon l’usage de la langue utilisée</span></li>
            <li><span>Saisir les mots clés les uns après les autres en les séparant par des virgules, une majuscule pour chaque mot clé</span></li>
            <li><span>Choisissez le ou les sujets correspondant(s) à votre thème de recherche dans le référentiel proposé dans la liste déroulante</span></li>
        </ul>
        <p class="aside-more-link">
            <a href="">En savoir plus</a>
        </p>
    </div>
</aside>
'

elements.contribution_help_aside_fichiers.name = "contribution_help_aside_fichiers"
elements.contribution_help_aside_fichiers.type = "Omeka\Form\Element\HtmlTextarea"
elements.contribution_help_aside_fichiers.options.element_group = "block_texts"
elements.contribution_help_aside_fichiers.options.label = "HTML pour l’aide de la contribution (fichiers)"
elements.contribution_help_aside_fichiers.attributes.id = "contribution_help_aside_fichiers"
elements.contribution_help_aside_fichiers.attributes.rows = "12"
elements.contribution_help_aside_fichiers.attributes.value = '<aside class="aside-interrogation">
    <div class="aside-content">
        <h4>Étape 3 : Téléchargez le(s) fichier(s) de votre travail de recherche</h4>
        <p class="aside-download-link">
            <a href="">Télécharger l’aide</a>
        </p>
        <ul>
            <li><span>Téléchargez le ou les fichiers au format pdf depuis votre ordinateur</span></li>
            <li><span>Précisez le nombre de pages</span></li>
            <li><span>Ne pas remplir les autres  informations (DUMAS, accès, embargo) qui seront complétées au moment de la validation de votre dépôt par votre « référent » DANTE selon les modalités d’accès définies sur votre autorisation de diffusion</span></li>
        </ul>
        <p class="aside-more-link">
            <a href="">En savoir plus</a>
        </p>
    </div>
</aside>
'

elements.contribution_help_aside_depot.name = "contribution_help_aside_depot"
elements.contribution_help_aside_depot.type = "Omeka\Form\Element\HtmlTextarea"
elements.contribution_help_aside_depot.options.element_group = "block_texts"
elements.contribution_help_aside_depot.options.label = "HTML pour l’aide de la contribution (dépôt)"
elements.contribution_help_aside_depot.attributes.id = "contribution_help_aside_depot"
elements.contribution_help_aside_depot.attributes.rows = "12"
elements.contribution_help_aside_depot.attributes.value = '<aside class="aside-interrogation">
    <div class="aside-content">
        <h4>Étape 4 : Prévisualisez et confirmez votre dépôt</h4>
        <p class="aside-download-link">
            <a href="">Télécharger l’aide</a>
        </p>
        <ul>
            <li><span>Vérifiez les données saisies et le(s) fichier(s) téléchargé(s)</span></li>
            <li><span>Pour modifier des informations, cliquez sur éditer pour revenir sur votre dépôt et faire les modifications nécessaires</span></li>
            <li><span>Si tout est correct cliquez sur confirmer le dépôt</span></li>
        </ul>
        <p class="aside-more-link">
            <a href="">En savoir plus</a>
        </p>
    </div>
</aside>
'

elements.contribution_help_aside_template_these.name = "contribution_help_aside_template_these"
elements.contribution_help_aside_template_these.type = "Omeka\Form\Element\HtmlTextarea"
elements.contribution_help_aside_template_these.options.element_group = "block_texts"
elements.contribution_help_aside_template_these.options.label = "HTML pour l’aide de la contribution (thèse) (type)"
elements.contribution_help_aside_template_these.attributes.id = "contribution_help_aside_template_these"
elements.contribution_help_aside_template_these.attributes.rows = "12"
elements.contribution_help_aside_template_these.attributes.value = '<aside class="aside-interrogation">
    <div class="aside-content">
        <h4>Étape 1 : Sélectionnez le type de document Thèse</h4>
        <p class="aside-download-link">
            <a href="">Télécharger l’aide</a>
        </p>
        <p class="aside-more-link">
            <a href="">En savoir plus</a>
        </p>
    </div>
</aside>
'

elements.contribution_help_aside_notice_these.name = "contribution_help_aside_notice_these"
elements.contribution_help_aside_notice_these.type = "Omeka\Form\Element\HtmlTextarea"
elements.contribution_help_aside_notice_these.options.element_group = "block_texts"
elements.contribution_help_aside_notice_these.options.label = "HTML pour l’aide de la contribution (thèse) (notice)"
elements.contribution_help_aside_notice_these.attributes.id = "contribution_help_aside_notice_these"
elements.contribution_help_aside_notice_these.attributes.rows = "12"
elements.contribution_help_aside_notice_these.attributes.value = '<aside class="aside-interrogation">
    <div class="aside-content">
        <h4>Étape 2 : Saisissez les informations correspondant à votre dépôt</h4>
        <p class="aside-download-link">
            <a href="">Télécharger l’aide</a>
        </p>
        <ul>
            <li><span>Les informations doivent être saisies en minuscules avec la 1ère lettre en majuscule selon l’usage de la langue utilisée</span></li>
            <li><span>Les résumés ne doivent pas excéder 4000 caractères espaces compris</span></li>
            <li><span>Saisir les mots clés les uns après les autres en les séparant par des virgules, une majuscule pour chaque mot clé </span></li>
            <li><span>Choisissez le(s) sujet(s) correspondant(s) à votre thème de recherche dans le référentiel proposé dans la liste déroulante</span></li>
        </ul>
        <p class="aside-more-link">
            <a href="">En savoir plus</a>
        </p>
    </div>
</aside>
'

elements.contribution_help_aside_fichiers_these.name = "contribution_help_aside_fichiers_these"
elements.contribution_help_aside_fichiers_these.type = "Omeka\Form\Element\HtmlTextarea"
elements.contribution_help_aside_fichiers_these.options.element_group = "block_texts"
elements.contribution_help_aside_fichiers_these.options.label = "HTML pour l’aide de la contribution (thèse) (fichiers)"
elements.contribution_help_aside_fichiers_these.attributes.id = "contribution_help_aside_fichiers_these"
elements.contribution_help_aside_fichiers_these.attributes.rows = "12"
elements.contribution_help_aside_fichiers_these.attributes.value = '<aside class="aside-interrogation">
    <div class="aside-content">
        <h4>Étape 3 : Téléchargez le(s) fichier(s) de votre travail de recherche</h4>
        <p class="aside-download-link">
            <a href="">Télécharger l’aide</a>
        </p>
        <ul>
            <li><span>Téléchargez le(s) fichier(s) au format natif, word par exemple, et au format pdf depuis votre ordinateur</span></li>
        </ul>
        <p class="aside-more-link">
            <a href="">En savoir plus</a>
        </p>
    </div>
</aside>
'

elements.contribution_help_aside_depot_these.name = "contribution_help_aside_depot_these"
elements.contribution_help_aside_depot_these.type = "Omeka\Form\Element\HtmlTextarea"
elements.contribution_help_aside_depot_these.options.element_group = "block_texts"
elements.contribution_help_aside_depot_these.options.label = "HTML pour l’aide de la contribution (thèse) (dépôt)"
elements.contribution_help_aside_depot_these.attributes.id = "contribution_help_aside_depot_these"
elements.contribution_help_aside_depot_these.attributes.rows = "12"
elements.contribution_help_aside_depot_these.attributes.value = '<aside class="aside-interrogation">
    <div class="aside-content">
        <h4>Étape 4 : Prévisualisez et confirmez votre dépôt</h4>
        <p class="aside-download-link">
            <a href="">Télécharger l’aide</a>
        </p>
        <ul>
            <li><span>Vérifiez les données saisies et le(s) fichier(s) téléchargé(s)</span></li>
            <li><span>Pour modifier des informations, cliquez sur éditer pour pouvoir revenir sur votre dépôt et faire les modifications nécessaires.</span></li>
            <li><span>Si tout est correct cliquez sur confirmer le dépôt</span></li>
        </ul>
        <p class="aside-more-link">
            <a href="">En savoir plus</a>
        </p>
    </div>
</aside>
'

page_templates.aside-page = "Aside page"
page_templates.simple-page = "Simple page"

block_templates.block.block-aside-sommaire = "Bloc Aside sommaire"

block_templates.browsePreview.browse-preview-home = "Browse preview Home"

block_templates.html.html-aside-exclamation = "Html exclamation"
block_templates.html.html-aside-interrogation = "Html interrogation"
block_templates.html.html-aside-sommaire = "Html sommaire"
block_templates.html.html-encadre-image = "Html encadré image"
